/*
Lee 10 enteros, pregunta un número para buscar y lo busca y dice SI o NO
*/

import java.util.Scanner;

public class Arrays5 {
   public static void main(String[] args) {

     Scanner entrada = new Scanner(System.in);

     int i, x;
     int j = -1;
     int[] a = new int[10];
     boolean found = false;

     System.out.println("Introduce 10 números en el array:");

     for (i=0; i < a.length; i++) {
       a[i] = entrada.nextInt();
     }

     System.out.println("Introduce un número para buscarlo en el array:");

     x = entrada.nextInt();

     for (i=0; i < a.length; i++) {
       if (a[i] == x) {
         found = true;
         j = i;
       }
     }

     if (found) {
       System.out.println("SI");
       System.out.println("Posición: "+j);
     }
     else System.out.println("NO");

   }
}
