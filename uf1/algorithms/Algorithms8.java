/*
Write a program that ask for 10 salaries and tell us how many employees they charge between $100 and $300 and how many they charge more than $300. In addition the program will have to show us the total amount of salaries of its employees.
*/

import java.util.Scanner;

public class Algorithms8 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x, less, more, sum;
      sum = less = more = 0;
      int times = 10;

      for (int i=0; i<times; i++) {
        do { // Read an integer until we get a valid salary
          x = in.nextInt();
        } while (x < 100);

        // When the do-while finish we have a valid salary in x
        if (x <= 300) {
          less++;
        } else {
          more++;
        }

        sum = sum + x;
      }

      System.out.println("Number of salaries less than 300: "+less);
      System.out.println("Number of salaries more than 300: "+more);
      System.out.println("Total amount of salaries is: "+sum);
   }
}
