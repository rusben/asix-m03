import java.util.Scanner;

public class Algorithms7 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x;
      int sum = 0;
      int times = 10;

      for (int i=0; i<times; i++) {
        do { // Read an integer until we get a valid height
          x = in.nextInt();
        } while (x < 0);
        sum = sum + x;
      }

      System.out.println("The average is: "+sum/times);
   }
}
