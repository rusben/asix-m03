/*
Write a program to fill a vector of 10 integers and show the sum of the values that are in positions of the vector multiple of 3
*/

import java.util.Scanner;

public class Algorithms13 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int i, x, sum;
      int[] a = new int[10];

      for (i=0; i<10; i++) {
        x = in.nextInt();
        // Asigno a la posición i del vector el elemento
        a[i] = x;
      }

    sum = 0;
    // Option 1
    for (i=0; i<10; i++) {
      if (i % 3 == 0) sum = sum + a[i];
    }
    System.out.println("The sum is: "+sum);

    sum = 0;
    // Option 2
    for (i=0; i<10; i+=3) {
        sum = sum + a[i];
    }

    System.out.println("The sum is: "+sum);
   }
}
