import java.util.Scanner;

public class Loops4 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int sum = 0;
      int count = 0;

      while (x >= 0) {
        sum = sum + x;
        x = in.nextInt();
        count++;
      }

      System.out.println("The sum is: "+sum);
      System.out.println("The number of numbers introduced is: "+count);
   }
}
