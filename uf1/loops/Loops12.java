/*
Lee una secuencia de 3 bits. Lee una línea de bits hasta encontrar un -1 e indica cuántas veces aparece la secuencia introducida.
La línea tiene al menos 3 elementos y un -1.

1 0 1
1 0 1 0 0 1 0 0 1 0 0 1 -1

1
*/

import java.util.Scanner;

public class Loops12 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int times = 0;
      int x, y, z;
      int i, j, k;

      x = in.nextInt();
      y = in.nextInt();
      z = in.nextInt();

      i = in.nextInt();
      j = in.nextInt();
      k = in.nextInt();

      while (k != -1) {

        if (x == i && y == j && z == k) {
          times++;
        }

        i = j;
        j = k;

        k = in.nextInt();
      }
      System.out.println(times);
   }
}
