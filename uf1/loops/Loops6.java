

import java.util.Scanner;

public class Loops6 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int i = 1;
      int sum = 0;

      while (i <= x) {
        sum = sum + i;
        i = i + 2; // Equivalente a i+=2;
      }

      System.out.println(sum);

   }
}
