
import java.util.Scanner;

public class StringsExample {
   public static void main(String[] args) {

   Scanner in = new Scanner(System.in);
   String line;
   char[] lineArray;

     while (in.hasNextLine()) {
       line = in.nextLine();

       // Elimina los espacios
       line = line.replace(" ", "");

       // Convierto la linea de String a array de carácteres
       lineArray = line.toCharArray();

      System.out.println("UNO: "+line);

       for (int i =0; i< lineArray.length; i++) {
         System.out.print("DOS: "+lineArray[i]+" ");
       }

       System.out.println();
       System.out.println("LONGITUD: "+line.length());

     }

     in.close();

   }
}
