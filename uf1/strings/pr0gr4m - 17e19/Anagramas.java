
  import java.util.Scanner;

  public class Anagramas {
     public static void main(String[] args) {

      Scanner entrada = new Scanner(System.in);
      char[] anag1;
      char[] anag2;
      String anagrama1 = entrada.nextLine();
      String anagrama2 = entrada.nextLine();
      anagrama1 = anagrama1.toLowerCase();
      anagrama2 = anagrama2.toLowerCase();
      anagrama1 = anagrama1.replace(" ", "");
      anagrama2 = anagrama2.replace(" ", "");
      anag1 = anagrama1.toCharArray();
      anag2 = anagrama2.toCharArray();

      // tienen la misma longitud
      if (anag1.length == anag2.length) {

        for (int i = 0; i < anag1.length; i++){
          for (int j = 0; j < anag1.length; j++){

             if (anag1[i] == anag2[j]){
               anag2[j] = ' ';
               break;
             }

          }
       }

       // Es anagrama si en el segundo vector sólo hay espacios
       boolean anagrama = true;
       for (int k = 0; k < anag2.length; k++){

         if (anag2[k] != ' '){
          anagrama = false;
          break;
         }

       }
       if (anagrama) System.out.println("SI");
       else System.out.println("NO");



      } else {
        System.out.println("NO");
      }

    }
  }
