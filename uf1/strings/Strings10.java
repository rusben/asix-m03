/*
Lee una secuencia de a's y b's y busca una subsecuencia de dos elementos dentro de la secuencia.
Imprime el número de veces que aparece y los índices del vector.
*/

import java.util.Scanner;

public class Strings10 {
   public static void main(String[] args) {

     Scanner in = new Scanner(System.in);
     String line;
     char[] lineArray;
     char first = 'a';
     char second = 'b';
     int count;
     String result;

     System.out.println("Introduce la subsecuencia (dos carácteres separados)");
     first = in.next(".").charAt(0); // Lee exactamente un carácter de la entrada
     second = in.next(".").charAt(0); // Lee exactamente un carácter de la entrada

     while (in.hasNextLine()) {
       result = "";
       count = 0;

       System.out.println("Introduce la secuencia:");
       line = in.nextLine();

       // Convierto la linea de String a array de carácteres
       lineArray = line.toCharArray();

       int j = 1;
       // Busca la subsecuencia
       for (int i =0; i < lineArray.length - 1; i++) {

         // Matching de la secuencia
         if (lineArray[i] == first && lineArray[j] == second) {
           count++;
           result = result + " " + i;
         }

         j++;

       }

       System.out.println(count+" - "+result);

     }

     in.close();

   }
}
