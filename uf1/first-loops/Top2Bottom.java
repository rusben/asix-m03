import java.util.Scanner;

public class Top2Bottom {
   public static void main(String[] args) {
      // This program reads two numbers x,y and writes all integers between x and y, or y and x, in decreasing order
      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int y = in.nextInt();

      if (x > y) {
        while (x >= y) {
          System.out.print(x + " ");
          x--; // Equivalente a x = x - 1;
        }
      } else { // y >= x
        while (y >= x) {
          System.out.print(y + " ");
          y--; // Equivalente a x = x - 1;
        }
      }
      System.out.println();
   }
}
