import java.io.*;
import java.util.*;

public class Fontana {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner in = new Scanner(System.in);
        
        int x = in.nextInt();
        int y = 0;
        
        int c5, c10, c20, c50, c100, c200;
        c5 = c10 = c20 = c50 = c100 = c200 = 0;
        
        do {
            
            if (x - y == 5) c5++; 
            if (x - y == 10) c10++;
            if (x - y == 20) c20++;
            if (x - y == 50) c50++;
            if (x - y == 100) c100++;
            if (x - y == 200) c200++;
            
            y = x;
            x = in.nextInt();
            
        } while (x != 0);
        
        System.out.println("5) "+c5);
        System.out.println("10) "+c10);
        System.out.println("20) "+c20);
        System.out.println("50) "+c50);
        System.out.println("100) "+c100);
        System.out.println("200) "+c200);
        
        
    }
}
