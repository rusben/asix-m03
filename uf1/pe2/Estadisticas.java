import java.io.*;
import java.util.*;

public class Estadisticas {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner in = new Scanner(System.in);
        
        
        int x = in.nextInt();   
        int y = in.nextInt();
        
        int l = 0;
        int v = 0;
                
        while (x != -1 && y != -1) {
            
            if (x == 0 && y == 0) System.out.println("EMPIEZA EL PARTIDO, EMPATE: "+x+" "+y);
            else {
                
                if (x > l) System.out.print("GOL LOCAL, ");
                else if (y > v) System.out.print("GOL VISITANTE, ");
                
                if (x > y) System.out.print("GANA LOCAL: ");
                else if (x < y) System.out.print("GANA VISITANTE: ");
                else System.out.print("EMPATE: ");
                
                System.out.print(x+" "+y);
                System.out.println();

            }
            
            
            l = x;
            v = y;
            
            x = in.nextInt();
            y = in.nextInt();
            


        }
        
        System.out.print("FINAL DEL PARTIDO, ");
        if (l > v) System.out.print("GANA LOCAL: ");
        else if (l < v) System.out.print("GANA VISITANTE: ");
        else System.out.print("EMPATE: ");
        System.out.print(l+" "+v);
        
    }
}
