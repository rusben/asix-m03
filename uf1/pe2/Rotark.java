import java.io.*;
import java.util.*;

public class Rotark {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner in = new Scanner (System.in);
        
        int n = in.nextInt();
        int[] elements = new int[n];
        
        for (int i = 0; i < elements.length; i++) {
            elements[i] = in.nextInt();
        }
        
        int k = in.nextInt();
                
        // Imprime los elementos desde v[k] hasta v[v.lenght-1]
        for (int j = k; j < elements.length; j++) {
            System.out.print(elements[j] + " ");
        }
        
        // Quedan elementos si k es diferente de 0
        if (k != 0) {
            // Imprime los elementos restantes
            for (int j = 0; j < k; j++) {
                System.out.print(elements[j] + " ");
            }   
        }
       
        
        
    }
}
