/*
Read a matrix and print the transposed
*/

import java.util.Scanner;

public class Transpose {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int y = in.nextInt();

      // Original
      int[][] m = new int[x][y];
      // Transpose
      int[][] t = new int[y][x];

      // Read the matrix
      for (int i=0; i<m.length; i++) {
        for (int j=0; j<m[0].length; j++) {
          m[i][j] = in.nextInt();
        }
      }

      // Transpose the matrix
      for (int i=0; i<m.length; i++) {
        for (int j=0; j<m[0].length; j++) {
          t[j][i] = m[i][j];
        }
      }

      System.out.println(t.length);
      System.out.println(t[0].length);

      // Print the matrix
      for (int i=0; i<t.length; i++) {
        for (int j=0; j<t[0].length; j++) {
          System.out.print(t[i][j]+ " ");
        }
        System.out.println();
      }

   }
}
