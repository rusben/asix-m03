/*
Read a matrix
*/

import java.util.Scanner;

public class Matrix {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int y = in.nextInt();
      int[][] m = new int[x][y];

      // Read the matrix
      for (int i=0; i<x; i++) {
        for (int j=0; j<y; j++) {
          m[i][j] = in.nextInt();
        }
      }

      // Read the matrix
      for (int i=0; i<x; i++) {
        for (int j=0; j<y; j++) {
          m[i][j] = in.nextInt();
        }
      }


      // Add 5 to each element
      for (int i=0; i<m.length; i++) {
        for (int j=0; j<m[0].length; j++) {
          m[i][j] = m[i][j] + 5;
        }
      }

      // Print the matrix
      for (int i=0; i<x; i++) {
          System.out.print("Imprimiendo fila "+(i+1)+": ");
        for (int j=0; j<y; j++) {
          System.out.print(m[i][j]+ " ");
        }
        System.out.println();
      }

      System.out.println();
      System.out.println("Fila: ");
      System.out.println(m.length);
      System.out.println("-----------------");
      System.out.println("Columna: ");
      System.out.println(m[1].length);

   }
}
