import java.util.Scanner;

public class E4{
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x;
      int i = 0;
      int count = -1;

      do{
        x = in.nextInt();
        if (x > i){
          count++;
        }

        i = x;

      } while (x != 0);
      System.out.println(count);

    }
  }
