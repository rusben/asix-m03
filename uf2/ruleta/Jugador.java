class Jugador {

  private int id;
  private String nombre;
  private boolean vivo;

  Jugador(String nombre, int id) {
    this.id = id;
    this.nombre = nombre;
    this.vivo = true;
  }

  int getId() {
      return this.id;
  }

  // retorna true si el jugadores está vivo
  boolean isAlive() {
    return this.vivo;
  }

  boolean disparar(Revolver r) {
      if (r.disparar()) {
        // Estaba la bala, has palmado
        this.vivo = false;
        return true;

      } else {
        // Te has salvado
        return false;
      }
  }
}

/*

- Jugador:
  Atributos
            id (representa el número del jugador, empieza en 1)
            nombre (Empezara con Jugador más su ID, “Jugador 1” por ejemplo)
            vivo (indica si está vivo o no el jugador)
  Funciones:
            disparar(Revolver r): el jugador se apunta y se dispara, si la bala se dispara, el jugador muere.

*/
