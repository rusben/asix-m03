import java.io.*;
import java.util.*;

class Revolver {

  private int currentPosition;
  private int bulletPosition;

  Revolver() {

    int max = 6;
    int min = 0;

    // create instance of Random class
    Random randomNum = new Random();
    this.currentPosition = min + randomNum.nextInt(max);
    this.bulletPosition = min + randomNum.nextInt(max);
  }

  // disparar(): devuelve true si la bala coincide con la posición actual

  boolean disparar() {
      if (this.currentPosition == this.bulletPosition) {
        siguienteBala();
        return true;
      } else {
        siguienteBala();
        return false;
      }
  }

  //siguienteBala(): cambia a la siguiente posición del tambor

  void siguienteBala() {
    this.currentPosition++;
    if (this.currentPosition > 5) this.currentPosition = 0;
  }

  //toString(): muestra información del revolver (posición actual y donde está la bala)
  public String toString() {
    return this.currentPosition+" "+this.bulletPosition;
  }




}

/*

- Revolver:
  Atributos:
      posición actual (posición del tambor donde se dispara, puede que esté la bala o no)
      posición bala (la posición del tambor donde se encuentra la bala)

Estas dos posiciones, se generaran aleatoriamente.

  Funciones:
        disparar(): devuelve true si la bala coincide con la posición actual
        siguienteBala(): cambia a la siguiente posición del tambor
        toString(): muestra información del revolver (posición actual y donde está la bala)

*/
