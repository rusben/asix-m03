
class Biblioteca {
  private String nombre;
  private int aforo;
  private Libro[] libros = new Libro[100];

  Biblioteca(String nombre, int aforo) {
    this.nombre = nombre;
    this.aforo = aforo;
    inicializaLibreria();
  }

  private void inicializaLibreria() {
    for (int i = 0; i < this.libros.length; i++) {
      this.libros[i] = null;
    }
  }

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAforo() {
		return this.aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

  @Override
  public String toString(){
    return "La Biblioteca "+this.getNombre()+" tiene un total de "+this.getCantidadLibros()+" Libros y un aforo máximo de "+this.getAforo()+" personas.";
  }

  public int getCantidadLibros() {
    int nLibros = 0;
    for (int i = 0; i < this.libros.length; i++) {
      if (this.libros[i] instanceof Libro) {
        nLibros++;
      }
    }
    return nLibros;
  }

  public void addLibro(Libro l) {

    int k = getUltimaPosicionLibre();

    if (k != -1) {
      this.libros[k] = l;
    } else {
      System.out.println("Biblioteca llena, no hay suficiente espacio.");
    }
  }

  public Libro buscarLibroPorISBN(char[] ISBN) {
    for (int i = 0; i < this.libros.length; i++) {
      if (areEquals(this.libros[i].getISBN(), ISBN)) return this.libros[i];
    }
    return null;
  }

  // Compara dos char[] y dice si son iguales o no
  private boolean areEquals(char[] a, char[] b) {

    if (a.length != b.length) return false;

    for (int i = 0; i < a.length; i++) {
      if (a[i] != b[i]) return false;
    }
    return true;
  }

  // Retorna la última posición libre dentro del vector de libros.
  // Si no existe ninguna posición libre retorna -1
  private int getUltimaPosicionLibre() {
    for (int i = 0; i < this.libros.length; i++) {
      if (this.libros[i] == null) return i;
    }
    return -1;
  }

}
