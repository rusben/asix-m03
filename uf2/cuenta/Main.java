
class Main {
    public static void main(String[] args) {

      // Creamos dos cuentas, una con cada constructor
      Cuenta c1 = new Cuenta("John");
      Cuenta c2 = new Cuenta("Rafael", 1000);

      // Ingresamos en ambas cuentas.
      c1.ingresar(1000);
      c2.ingresar(3000);

      // Comprobamos el estado de las cuentas
      System.out.println(c1);
      System.out.println(c2);

      // Retiramos de ambas cuentas
      c1.retirar(1001);
      c2.retirar(2000);

      // Comprobamos el estado de las cuentas
      System.out.println(c1);
      System.out.println(c2);

    }
}
