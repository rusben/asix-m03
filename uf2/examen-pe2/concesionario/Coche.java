class Coche {

  private String modelo;
  private Marca marca;
  private String matricula;
  private int km;
  private float precio;
  private int estado;

  Coche(String modelo, Marca marca, String matricula, int km, float precio, int estado) {
    this.modelo = modelo;
    this.marca = marca;
    this.matricula = matricula;
    this.km = km;
    this.precio = precio;

    this.estado = estado;
    if (estado > 10) this.estado = 10;
    if (estado < 0) this.estado = 0;
  }

  public String getMatricula() {
    return this.matricula;
  }

  public Marca getMarca() {
    return this.marca;
  }

}
