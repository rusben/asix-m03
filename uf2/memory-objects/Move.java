import java.io.*;
import java.util.*;

public class Move {

  private int x;
  private int y;

  public static Move getMove() {

    Scanner in = new Scanner(System.in);

    int x = in.nextInt();
    int y = in.nextInt();

    Move v = new Move();
    v.setX(x);
    v.setY(y);

    return v;

  }

  public int getX() {
    return this.x;
  }

  public void setX(int value) {
    this.x = value;
  }

  public int getY() {
    return this.y;
  }

  public void setY(int value) {
    this.y = value;
  }

  /**
   * Indica si las coordendas a = {x,y} b = {x,y} son coordeandas correctas
   * dentro de la matriz cards
   *
   * @param ax Coordenada x de la primera card a
   * @param ay Coordenada y de la primera card a
   * @param bx Coordenada x de la primera card b
   * @param by Coordenada y de la primera card b
   *
   * @return  true si si las coordendas a = {x,y} b = {x,y} son coordeandas
   *               correctas estando dentro del límite de cards
   *          false en caso contrario.
   */
  public static Boolean validMove(Board b, int ax,int ay) {

    if (ax >= 0 && ax < b.getRows() && ay >= 0 && ay < b.getCols()) {
      return true;
    }

    return false;
  }



}
