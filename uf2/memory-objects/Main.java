import java.io.*;
import java.util.*;

public class Main {

  public static void main(String[] args) {

    Board b = new Board();

    // Inicializamos el tablero
    b.initializeBoard();

    // Mostramos el tablero
    b.showBoard();

    // Esperar 5 segundos y empezar
    b.wait(5);
    b.clear();

    // Printamos el tablero
    b.printBoard();

    // Bucle del juego
    // Mientras el juego no haya acabado
    while (!b.endGame()) {

      Move m1 = Move.getMove();
      Move m2 = Move.getMove();

      System.out.println(m1.getX());
      System.out.println(m1.getY());
      System.out.println(m2.getX());
      System.out.println(m2.getY());


        // Si la coordenada es válida, realizar la jugada
        if (Move.validMove(b, m1.getX()-1, m1.getY()-1) && Move.validMove(b, m2.getX()-1, m2.getY()-1)) {
          b.showAndLetItBeIfDifferent(m1.getX()-1, m1.getY()-1, m2.getX()-1, m2.getY()-1);
        }

        b.wait(3);
        b.clear();
        b.printBoard();

     }

     // Acaba el bucle porque ha habido un ganador o empate
     b.printResult();



  }
}
