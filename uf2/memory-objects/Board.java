import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.*;

public class Board {

  private static Card[][] cards = new Card[4][4];

  /**
   * Inicializa el tablero board, crea todas las cartas del tablero, les da
   * un valor y las setea inicialmente como giradas
   *
   * @return void
   */
  public void initializeBoard() {

    int value = 1;

    // Crear todas las cartas
    for (int i = 0; i < cards.length; i++) {
      for (int j = 0; j < cards[0].length; j++) {

          Card c = new Card(value, true);
          cards[i][j] = c;

          if (j % 2 != 0) value++;
      }
    }

    // Mezcla el tablero
    mixBoard();

  }

  /**
   * Mezcla el tablero
   *
   * @return void
   */
  public void mixBoard() {
    // Realizar 25 intercambios entre las posiciones de la matriz
    int intercambios = 25;
    int max = cards[0].length;
    int min = 0;

    // create instance of Random class
    Random randomNum = new Random();

    for (int i=0; i < intercambios; i++) {

      int ax = min + randomNum.nextInt(max);
      int ay = min + randomNum.nextInt(max);

      int bx = min + randomNum.nextInt(max);
      int by = min + randomNum.nextInt(max);

      swap(ax, ay, bx, by);

    }
  }


  /**
   * Imprime por la salida estándar el estado actual del tablero cards.
   * * - Carta girada
   * Número entre 1 y 8 - Carta descubierta
   *
   * @return void
   */
  public void showBoard() {
    printLine();
    for (int i = 0; i < cards.length; i++) {
      for (int j = 0; j < cards[0].length; j++) {
        System.out.print(cards[i][j].getValue()+" ");
      }
        System.out.println();
    }
    printLine();
  }

  /**
   * Intercambia los valores a = {x,y} b = {x,y} en la matriz cards
   *
   * @return void
   */
  public void swap(int ax, int ay, int bx, int by) {
    Card tmp = cards[ax][ay];
    cards[ax][ay] = cards[bx][by];
    cards[bx][by] = tmp;
  }

  /**
   * Imprime por la salida estándar el estado actual del tablero cards.
   * * - Carta girada
   * Número entre 1 y 8 - Carta descubierta
   *
   * @return void
   */
  public void printBoard() {
    printLine();
    for (int i = 0; i < cards.length; i++) {
      for (int j = 0; j < cards[0].length; j++) {
        if (cards[i][j].getHidden()) System.out.print("* ");
        else System.out.print(cards[i][j].getValue()+" ");
      }
        System.out.println();
    }
    printLine();
  }

  /**
   * Imprime por la salida estándar el resultado de la partida.
   *
   * @return  void
   */
  public void printResult() {
    System.out.println("You lose!");
  }

  public void printLine() {
    System.out.println("----------");
  }

  /**
   * Clear the terminal screen
   *
   * @return  void
   */
  public void clear() {
    System.out.print("\033[H\033[2J");
  }

  /**
   * Wait n seconds
   *
   * @return  void
   */
  public void wait(int n) {

    try {
      TimeUnit.SECONDS.sleep(n);
    } catch(InterruptedException ex) {
      // Do something
    }
  }

  /**
   * Indica si el tablero hidden tiene todas las cartas mostradas (todas las
   * posiciones a false)
   *
   * @return  true si el tablero hidden tiene todas las posiciones a false
   *          false en caso contrario.
   */

  public Boolean endGame() {

    for (int i = 0; i < cards.length; i++) {
      for (int j = 0; j < cards[0].length; j++) {
          if (cards[i][j].getHidden()) return false;
      }
    }

    return true;
  }

  public int getCols() {
    return cards[0].length;
  }

  public int getRows() {
    return cards.length;
  }


  /**
   * Cambia en la matriz hidden de las cartas referenciadas por la coordeandas
   * a = {x,y} y b = {x,y} a descubiertas (valor false)
   *
   * @return  void
   */
  public void showAndLetItBeIfDifferent(int ax, int ay, int bx, int by) {

    boolean initialA = cards[ax][ay].getHidden();
    boolean initialB = cards[bx][by].getHidden();

    cards[ax][ay].setHidden(false);
    cards[bx][by].setHidden(false);

    printBoard();

    if (cards[ax][ay].getValue() != cards[bx][by].getValue()) {
      cards[ax][ay].setHidden(initialA);
      cards[bx][by].setHidden(initialB);
    }
  }
}
