class Carta {

  // Una carta tiene un número entre 1 y 12 (el 8 y el 9 no los incluimos) y un palo (espadas, bastos, oros y copas)
  private int numero;
  private String palo;
  private boolean mostrada;

	/**
	* Default Carta constructor
	*/
	public Carta(int numero, String palo) {
		this.numero = numero;
		this.palo = palo;
    this.mostrada = false;

	}

	/**
	* Returns value of numero
	* @return
	*/
	public int getNumero() {
		return numero;
	}

	/**
	* Sets new value of numero
	* @param
	*/
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	* Returns value of palo
	* @return
	*/
	public String getPalo() {
		return palo;
	}

	/**
	* Sets new value of palo
	* @param
	*/
	public void setPalo(String palo) {
		this.palo = palo;
	}

	/**
	* Create string representation of Carta for printing
	* @return
	*/
	@Override
	public String toString() {
		return this.numero + " " + this.palo;
	}



	/**
	* Returns value of mostrada
	* @return
	*/
	public boolean isMostrada() {
		return mostrada;
	}

	/**
	* Sets new value of mostrada
	* @param
	*/
	public void setMostrada(boolean mostrada) {
		this.mostrada = mostrada;
	}
}
