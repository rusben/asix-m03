package net.xeill.elpuig.model;

import java.util.Date;

public class Journey {

  private int id;
  private Date date;

  public Journey(int id, Date date){
        this.id = id;
        this.date = date;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int value) {
    this.id = value;
  }

  public int getDate() {
    return this.date;
  }

  public void setDate(Date value) {
    this.date = value;
  }
}
