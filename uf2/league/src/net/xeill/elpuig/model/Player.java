package net.xeill.elpuig.model;

public class Player {

  private int id;
  private String name;
  private int age;
  private Boolean amputated;
  private int teamId;

  public Player(int id, String name, int age, Boolean amputated, int teamId){
    this.id = id;
    this.name = name;
    this.age = age;
    this.amputated = amputated;
    this.teamId = teamId;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int value) {
    this.id = value;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String value) {
    this.name = value;
  }

  public int getAge() {
    return this.age;
  }

  public void setAge(int value) {
    this.age = value;
  }

  public Boolean getAmputated() {
    return this.amputated;
  }

  public void setAmputated(Boolean value) {
    this.amputated = value;
  }

  public int getTeamId() {
    return this.teamId;
  }

  public void setTeamId(int value) {
    this.teamId = value;
  }

}
