package net.xeill.elpuig.controller;

import net.xeill.elpuig.model.*;

public class PlayerController {

  static Player[] players = new Player[100];

  public static Player createPlayer(String name, int age, Boolean amputated, int teamId) {

      int id = nextId();

      if (id != -1) {
        Player p = new Player(id, name, age, amputated, teamId);
        players[id] = p;
        return p;
      } else {
          // ERROR, imposible crear el player, out of space
          return null;
      }

  }

  public static Player updatePlayer(int playerId, String name, int age, Boolean amputated, int teamId) {

    // Buscar al player en players y modificarlo
    Player p = getPlayerById(playerId);

    p.setName(name);
    p.setAge(age);
    p.setAmputated(amputated);
    p.setTeamId(teamId);

    return p;

  }

  public static Boolean deletePlayer(int playerId) {
    // Buscar al player en players y eliminarlo

    if (playerId >= 0 && playerId < players.length) {
      players[playerId] = null;
      return true;
    } else {
        // ERROR
        return false;
    }
  }

  private static int nextId() {
    for (int i = 0; i < players.length; i++) {
        if (players[i] == null) return i;
    }
    return -1;
  }

  public static Player getPlayerById(int id) {
    for (int i = 0; i < players.length; i++) {
      if (players[i] != null) {
        if (players[i].getId() == id) return players[i];
      }
    }
    return null;
  }

  public static Player[] getPlayersByTeamId(int id) {

    Player[] playersTeam = new Player[10];
    int j = 0;

    for (int i = 0; i < players.length; i++) {
      if (players[i] != null) {
        if (players[i].getTeamId() == id) {
          playersTeam[j] = players[i];
          j++;
        }
      }
    }
    return playersTeam;
  }

}
