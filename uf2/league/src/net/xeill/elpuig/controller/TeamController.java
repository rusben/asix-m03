package net.xeill.elpuig.controller;

import net.xeill.elpuig.model.*;

public class TeamController {

  static Team[] teams = new Team[10];

  public Team createTeam(String name, int fundationYear) {

      int id = nextId();

      if (id != -1) {
        Team t = new Team(id, name, fundationYear);
        this.teams[id] = t;
        return t;
      } else {
          // ERROR, imposible crear el team, out of space
          return null;
      }

  }

  public Team updateTeam(int teamId, String name, int fundationYear) {

    // Buscar al team en teams y modificarlo
    Team t = getTeamById(teamId);

    t.setName(name);
    t.setFundationYear(fundationYear);

    return t;

  }

  public Boolean deleteTeam(int teamId) {

    if (teamId >= 0 && teamId < teams.length) {
      this.teams[teamId] = null;
      return true;
    } else {
        // ERROR
        return false;
    }
  }

  public static Team[] getTeams() {
    return  teams;
  }

  private static int nextId() {
    for (int i = 0; i < teams.length; i++) {
        if (teams[i] == null) return i;
    }
    return -1;
  }

  public static Team getTeamById(int id) {
    for (int i = 0; i < teams.length; i++) {
      if (teams[i] != null) {
        if (teams[i].getId() == id) return teams[i];
      }
    }
    return null;
  }


}
