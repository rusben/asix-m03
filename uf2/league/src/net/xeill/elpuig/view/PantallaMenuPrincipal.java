package net.xeill.elpuig.view;

 import net.xeill.elpuig.view.widget.LectorTeclat;
 import net.xeill.elpuig.view.widget.Missatge;

import java.util.Scanner;

public class PantallaMenuPrincipal {
    public static void mostrar(){
        while(true) {
            Missatge.mostrarTitol("PETANCA");
            System.out.println("a) Players");
            System.out.println("b) Teams");
            System.out.println("c) Matches");
            System.out.println("b) Journeys");
            System.out.println("*) Exit");
            String opcio = LectorTeclat.llegirOpcio();

            switch (opcio) {
                case "a":
                    PantallaMenuPlayers.mostrar();
                    break;
                case "b":
                  //  PantallaMenuEquips.mostrar();
                    break;
                default:
                    return;
            }
        }
    }
}
