/*
Implementa el método Equation.calculateSolution()
Este método debe almacenar en la variable 'x' la solucion de la ecuacion:
    ax + b = 0
 */

class Equation {
   float a, b;
   float x;

   void calculateSolution() {
     x = -b / a;
   }
}

public class E5 {

   public static void main(String[] args) {
	    Equation equation = new Equation();

	    equation.a = 5;
	    equation.b = 10;

	    equation.calculateSolution();

       System.out.println(equation.x);

   }
}
