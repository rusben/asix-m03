/*
Implementa el método Equation2D.solve()
    debe retornar un array de dos float con las soluciones a la ecuacion:

    ax² + bx + c = 0
*/

//class Equation2D {
//
//    float a, b, c;
//}
//
//public class E10 {
//
//    public static void main(String[] args) {
//
//        Equation2D equation2D = new Equation2D();
//
//        equation2D.a = 10;
//        equation2D.b = 4;
//        equation2D.c = 10;
//
//        float[] x = equation2D.solve();
//
//        System.out.println("Solucion: " + x[0] + " y " + x[1]);
//    }
//}
