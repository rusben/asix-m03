/*
Implementa los métodos switchOn() y switchOff() de la clase Led.
    switchOn() cambia la variable 'state' a 'true'
    switchOff() cambia la variable 'state' a 'false'
 */

class Led {
  boolean state;

  void switchOn() {
    state = true;
  }

  void switchOff() {
    state = false;
  }

  void draw(){
       if(state){
           System.out.println("\033[92m*\033[0m");
       } else {
           System.out.println("*");
       }
   }
}


public class E1 {

   public static void main(String[] args) {
       Led l1 = new Led();
       Led l2 = new Led();

       System.out.println("------");
       l1.draw();
       l2.draw();

       l1.switchOn();

       System.out.println("------");
       l1.draw();
       l2.draw();

       l2.switchOn();

       System.out.println("------");
       l1.draw();
       l2.draw();

       l1.switchOff();

       System.out.println("------");
       l1.draw();
       l2.draw();
   }
}
