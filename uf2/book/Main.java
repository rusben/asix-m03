
class Main {
    public static void main(String[] args) {

      // Creamos varios Libros
      //Libro(char[] ISBN, String titulo, String autor, int nPaginas)

      Libro l1 = new Libro("1234567890".toCharArray(), "Las apasionantes clases de programación", "Rusben y el alumnado", 2);

      System.out.println(l1);

      Libro l2 = new Libro("1234567890".toCharArray(), "Terroríficas clases de programación", "Alumnos enfurecidos", 3);

      System.out.println(l2);

      if (l1.getNPaginas() > l2.getNPaginas()) {
        System.out.println("El libro con más páginas es "+l1.getTitulo());
      } else if (l2.getNPaginas() > l1.getNPaginas()) {
        System.out.println("El libro con más páginas es "+l2.getTitulo());
      } else {
        System.out.println("Los libros l1 y l2 tienen el mismo número de páginas.");
      }

    }
}
