import java.io.*;
import java.util.*;

class Judge {

  // Definición de la variable que representa el turno del jugador
  static int turn = 1;

  int getTurn() {
    return turn;
  }

  /**
   * Indica si el tablero board está lleno o no. Que el tablero está lleno significa
   * que no exite ninguna casilla en la que haya un 0.
   *
   * @return  true si el tablero está lleno, false en caso contrario.
   */

  public static Boolean fullBoard(Board b) {
    for (int i = 0; i < b.board.length; i++) {
      for (int j = 0; j < b.board[0].length; j++) {
        if (b.board[i][j] == 0) return false;
      }
    }
    return true;
  }

  /**
   * Indica si la coordenda x, y es una coordenada en la que se puede realizar
   *  un movimiento
   *
   * @param x La fila x del tablero en la que se realiza el movimiento
   * @param y La columna y del tablero en la que se realiza el movimiento
   *
   * @return  true si la coordenada  x, y, está del límite del tablero board y
   *               tiene un 0 (está vacía)
   *          false en caso contrario.
   */
  public static Boolean validPlay(int x, int y, Board b) {

    if (x >= 1 && x <= 3 && y >= 1 && y <= 3) {
      // Si la coordenada es correcta (dentro del límite)
      // Comprobamos que haya un 0 en esa posición
      if (b.board[x-1][y-1] == 0) return true;
    }

    return false;
  }

  /**
   * Indica si la coordenda tirada.x, tirada.y es una coordenada en la que se
   * puede realizar un movimiento
   *
   * @param tirada La coordenada x, y del tablero en la que se realiza el
   *               movimiento.
   *
   * @return  true si la coordenada  x, y, está del límite del tablero board y
   *               tiene un 0 (está vacía)
   *          false en caso contrario.
   */
  public static Boolean validPlay(Coordenate tirada, Board b) {
    if (tirada.x >= 1 && tirada.x <= 3 && tirada.y >= 1 && tirada.y <= 3) {
      // Si la coordenada es correcta (dentro del límite)
      // Comprobamos que haya un 0 en esa posición
      if (b.board[tirada.x-1][tirada.y-1] == 0) return true;
    }

    return false;
  }

  /**
   * Cambia el turno del juego.
   * Si el turno actual es 1 (tira el jugador 1), el turno turn pasa a ser 2.
   * Si el turno actual es 2 (tira el jugador 2), el turno turn pasa a ser 1.
   *
   * @return  void
   */
   public static void changeTurn() {
     if (turn == 1) turn = 2;
     else if (turn == 2) turn = 1;
  }


  /**
   * Indica si existe ganador en la posición actual de board.
   *
   * @return  0 si no hay ganador
   *          1 si gana el jugador 1
   *          2 si gana el jugador 2
   */
  public static int winner(Board b) {

    // Filas
    if (b.board[0][0] == b.board[0][1] && b.board[0][1] == b.board[0][2] && b.board[0][0] != 0) return b.board[0][0];
    if (b.board[1][0] == b.board[1][1] && b.board[1][1] == b.board[1][2] && b.board[1][0] != 0) return b.board[1][0];
    if (b.board[2][0] == b.board[2][1] && b.board[2][1] == b.board[2][2] && b.board[2][0] != 0) return b.board[2][0];

    // Columnas
    if (b.board[0][0] == b.board[1][0] && b.board[1][0] == b.board[2][0] && b.board[0][0] != 0) return b.board[0][0];
    if (b.board[0][1] == b.board[1][1] && b.board[1][1] == b.board[2][1] && b.board[0][1] != 0) return b.board[0][1];
    if (b.board[0][2] == b.board[1][2] && b.board[1][2] == b.board[2][2] && b.board[0][2] != 0) return b.board[0][2];

    // Diagonales
    if (b.board[0][0] == b.board[1][1] && b.board[1][1] == b.board[2][2] && b.board[0][0] != 0) return b.board[0][0];
    if (b.board[0][2] == b.board[1][1] && b.board[1][1] == b.board[2][0] && b.board[1][1] != 0) return b.board[1][1];

    return 0;
  }

}
