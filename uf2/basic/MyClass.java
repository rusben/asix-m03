public class MyClass {
  int x = 5;
  Boolean isPrime = true;

  public static void main(String[] args) {
    int[][]  matrix = new int[3][3];

    MyClass myObj1 = new MyClass();  // Object 1
    MyClass myObj2 = new MyClass();  // Object 2

    myObj1.x = 12;
    myObj1.isPrime = false;

    System.out.println(myObj1.x);
    System.out.println(myObj1.isPrime);

    System.out.println(myObj2.x);
    System.out.println(myObj2.isPrime);
  }
}
