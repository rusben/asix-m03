Pe1 - Classes i objectes


Estudiant (COGNOMS, NOM): 
Professors:
Rubén Arroyo
rusben@elpuig.xeill.net 
Desarrollo del examen
El examen se realiza con los ordenadores del centro.
Este examen se realiza de forma individual.
Cualquier intento de copia será castigado con la expulsión inmediata de la prueba.
Se deben entregar 4 ficheros .java Con los siguientes nombres: 
Serie.java
Videojuego.java
ElPuigMedia.java
Main.java

Crea una clase llamada Serie con las siguientes características: (2 puntos)

Sus atributos son título, número de temporadas, entregado, género y creador.

Por defecto, el número de temporadas es de 3 temporadas y entregado false. El resto de atributos serán valores por defecto según el tipo del atributo.

Los constructores que se implementaran serán:
Un constructor por defecto. (Sin parámetros)
Un constructor con título y creador. El resto los valores por defecto.
Un constructor con todos los atributos, excepto de entregado.

Los métodos que se implementara serán:
Métodos get de todos los atributos, excepto de entregado.
Métodos set de todos los atributos, excepto de entregado.
Sobrescribe el método toString con el siguiente formato:


Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Temporadas:
<VALOR_ATRIBUTO_TEMPORADAS>
Creador:
<VALOR_ATRIBUTO_CREADOR>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)





Crea una clase Videojuego con las siguientes características: (2 puntos)
Sus atributos son título, horas estimadas, entregado, género y compañía.

Por defecto, las horas estimadas serán 10 horas y entregado false. El resto de atributos serán valores por defecto según el tipo del atributo.

Los constructores que se implementaran serán:
Un constructor por defecto.
Un constructor con el título y horas estimadas. El resto por defecto.
Un constructor con todos los atributos, excepto de entregado.

Los métodos que se implementarán serán:
Métodos get de todos los atributos, excepto de entregado.
Métodos set de todos los atributos, excepto de entregado.
Sobrescribe el método toString con el siguiente formato: 

Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Compañía:
<VALOR_ATRIBUTO_COMPAÑIA>
Horas estimadas:
<VALOR_ATRIBUTO_HORAS_ESTIMADAS>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)

AB) Para la clase Serie y la clase Videojuego:
    entregar(): cambia el atributo entregado a true.
    devolver(): cambia el atributo entregado a false.
    isEntregado(): devuelve el estado del atributo entregado.

Antes de continuar prueba tus clases:
Con un método main realiza las siguientes pruebas:
Crea algunos Videojuegos y algunas Series.
Modifica sus valores con los métodos get y set.
Entrega algunos Videojuegos y algunas Series con el método entregar().
Devuelve algunos Videojuegos y algunas Series con el método devolver().
Comprueba que los métodos toString devuelven los valores esperados.


C) Implementa la clase ElPuigMedia:  (5 puntos)
La classe ElPuigMedia representa un servicio de préstamo de productos digitales, en nuestro caso Videojuegos y Series.

Los atributos de esta clase corresponden a dos arrays, uno de Series y otro de Videojuegos, de 10 posiciones cada uno. Además tendrá un atributo que indicará el tiempo máximo de los préstamos en días, por defecto 10 días.

Los constructores que se implementarán serán:
Un constructor por defecto. (Sin parámetros)
Un constructor con los dos arrays uno de Series y otro de videojuegos. El resto los valores por defecto.
Un constructor con todos los atributos.

Los métodos que se implementara serán:
Métodos get de todos los atributos.
Métodos set de todos los atributos.
añadirVideojuego(Videojuego: vj): Añade un Videojuego a la colección de Videojuegos.
añadirSerie(Serie: s): Añade una Serie a la colección de Series.
entregarSerie(int: i): Entrega la Serie en la posición i del array.
devolverSerie(int: i): Devuelve la Serie en la posición i del array.
entregarVideojuego(int: i): Entrega el Videojuego en la posición i del array.
devolverVideojuego(int: i): Devuelve el Videojuego en la posición i del array.
totalSeriesEntregadas(): Devuelve el total de Series entregadas.
totalVideojuegosEntregados(): Devuelve el total de Videojuegos entregados.
getVideoJuegoConMasHoras(): Devuelve el Videojuego con más horas estimadas.
getSerieConMasTemporadas(): Devuelve la Serie que tiene más temporadas.
Sobrescribe el método toString con el siguiente formato:

--------------------------- EL PUIG MEDIA ---------------------------
***********************
Tiempo máximo préstamos: <VALOR_ATRIBUTO_DIAS> días
***********************
********
Catálogo:
********
***********
Videojuegos:
***********
Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Compañía:
<VALOR_ATRIBUTO_COMPAÑIA>
Horas estimadas:
<VALOR_ATRIBUTO_HORAS_ESTIMADAS>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)

******
Series:
******
Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Temporadas:
<VALOR_ATRIBUTO_TEMPORADAS>
Creador:
<VALOR_ATRIBUTO_CREADOR>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)


* Llama a los métodos toString para imprimir el catálogo.

D) Implementa una clase Main  (1 punto)

Crea ahora una clase Main que realice lo siguiente:
Crea un objeto ElPuigMedia con el constructor por defecto y otro con el constructor con todos los atributos. (Aprovecha las pruebas que hiciste anteriormente).

Para cada uno de los objetos ElPuigMedia:
Entrega algunos Videojuegos y algunas Series con los métodos entregarSerie() y entregarVideojuego.
Cuenta cuántas Series y Videojuegos hay entregados e imprime sus valores. Después devuelve algunos con los métodos devolverVideojuego() y devolverSerie().
Vuelve a contar cuántas Series y Videojuegos hay entregados e imprime sus valores.
Por último, indica qué Videojuego tiene más horas estimadas y la Serie con más temporadas. Utiliza los métodos getVideoJuegoConMasHoras() y getSerieConMasTemporadas().
Muéstralos en pantalla con toda su información usando el método toString().

