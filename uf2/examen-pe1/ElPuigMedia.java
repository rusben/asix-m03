class ElPuigMedia {

  private Serie[] series;
  private Videojuego[] videojuegos;
  private int tiempoPrestamos;

  //Un constructor por defecto. (Sin parámetros)
	public ElPuigMedia() {
    initSeries();
    initVideojuegos();
    this.tiempoPrestamos = 10;
	}

  //Un constructor con los dos arrays uno de Series y otro de videojuegos. El resto los valores por defecto.
  public ElPuigMedia(Serie[] series, Videojuego[] videojuegos) {
    this.series = series;
    this.videojuegos = videojuegos;
    this.tiempoPrestamos = 10;
  }

  //Un constructor con todos los atributos.
	public ElPuigMedia(Serie[] series, Videojuego[] videojuegos, int tiempoPrestamos) {
    this.series = series;
    this.videojuegos = videojuegos;
    this.tiempoPrestamos = tiempoPrestamos;
	}

  private void initVideojuegos() {
    this.videojuegos = new Videojuego[10];
    for (int i = 0; i < this.videojuegos.length; i++) {
      this.videojuegos[i] = null;
    }
  }

  private void initSeries() {
    this.series = new Serie[10];
    for (int i = 0; i < this.series.length; i++) {
      this.series[i] = null;
    }
  }

	/**
	* Returns value of series
	* @return
	*/
	public Serie[] getSeries() {
		return this.series;
	}

	/**
	* Sets new value of series
	* @param
	*/
	public void setSeries(Serie[] series) {
		this.series = series;
	}

	/**
	* Returns value of videojuegos
	* @return
	*/
	public Videojuego[] getVideojuegos() {
		return this.videojuegos;
	}

	/**
	* Sets new value of videojuegos
	* @param
	*/
	public void setVideojuegos(Videojuego[] videojuegos) {
		this.videojuegos = videojuegos;
	}

	/**
	* Returns value of tiempoPrestamos
	* @return
	*/
	public int getTiempoPrestamos() {
		return this.tiempoPrestamos;
	}

	/**
	* Sets new value of tiempoPrestamos
	* @param
	*/
	public void setTiempoPrestamos(int tiempoPrestamos) {
		this.tiempoPrestamos = tiempoPrestamos;
	}

  /**
  * añadirVideojuego(Videojuego: vj): Añade un Videojuego a la colección de Videojuegos.
  */
  public void anadirVideojuego(Videojuego v) {
    for (int i = 0; i < this.videojuegos.length; i++) {
      if (this.videojuegos[i] == null) {
        this.videojuegos[i] = v;
        return;
      }
    }
  }

  public void anadirSerie(Serie s) {
    for (int i = 0; i < this.series.length; i++) {
      if (this.series[i] == null) {
        this.series[i] = s;
        return;
      }
    }
  }

  public void entregarSerie(int i) {
    if (this.series[i] != null) {
      this.series[i].entregar();
    }
  }

  public void devolverSerie(int i) {
    if (this.series[i] != null) {
      this.series[i].devolver();
    }
  }

  public void entregarVideojuego(int i) {
    if (this.videojuegos[i] != null) {
      this.videojuegos[i].entregar();
    }
  }

  public void devolverVideojuego(int i) {
    if (this.videojuegos[i] != null) {
      this.videojuegos[i].devolver();
    }
  }

  public int totalSeriesEntregadas() {
    int total = 0;
    for (int i = 0; i < this.series.length; i++) {
      if (this.series[i] != null) {
        if (this.series[i].isEntregado()) {
          total++;
        }
      }
    }

    return total;
  }

  public int totalVideojuegosEntregados() {
    int total = 0;
    for (int i = 0; i < this.videojuegos.length; i++) {
      if (this.videojuegos[i] != null) {
        if (this.videojuegos[i].isEntregado()) {
          total++;
        }
      }
    }

    return total;
  }

  public Videojuego getVideoJuegoConMasHoras() {
    Videojuego v = null;
    int max = 0;

    for (int i = 0; i < this.videojuegos.length; i++) {
      if (this.videojuegos[i] != null) {
        if (this.videojuegos[i].getHorasEstimadas() > max) {
          v = this.videojuegos[i];
          max = this.videojuegos[i].getHorasEstimadas();
        }
      }
    }

    return v;
  }

  public Serie getSerieConMasTemporadas() {
    Serie s = null;
    int max = 0;

    for (int i = 0; i < this.videojuegos.length; i++) {
      if (this.series[i] != null) {
        if (this.series[i].getNTemporadas() > max) {
          s = this.series[i];
          max = this.series[i].getNTemporadas();
        }
      }
    }

    return s;
  }


/*

añadirVideojuego(Videojuego: vj): Añade un Videojuego a la colección de Videojuegos.
añadirSerie(Serie: s): Añade una Serie a la colección de Series.
entregarSerie(int: i): Entrega la Serie en la posición i del array.
devolverSerie(int: i): Devuelve la Serie en la posición i del array.
entregarVideojuego(int: i): Entrega el Videojuego en la posición i del array.
devolverVideojuego(int: i): Devuelve el Videojuego en la posición i del array.
totalSeriesEntregadas(): Devuelve el total de Series entregadas.
totalVideojuegosEntregados(): Devuelve el total de Videojuegos entregados.
getVideoJuegoConMasHoras(): Devuelve el Videojuego con más horas estimadas.
getSerieConMasTemporadas(): Devuelve la Serie que tiene más temporadas.


*/

	/**
	* Create string representation of ElPuigMedia for printing
	* @return
	*/
	@Override
	public String toString() {
		// return "ElPuigMedia [e=" + e + ", o=" + o + ", tiempoPrestamos=" + tiempoPrestamos + "]";

    String s = "--------------------------- EL PUIG MEDIA ---------------------------\n"+
    "***********************\n"+
    "Tiempo máximo préstamos: "+this.tiempoPrestamos+" días\n"+
    "***********************\n"+
    "********\n"+
    "Catálogo:\n"+
    "********\n"+
    "***********\n"+
    "Videojuegos:\n"+
    "***********\n";

    for (int i = 0; i < this.videojuegos.length; i++) {
      if (this.videojuegos[i] != null) s+= this.videojuegos[i].toString();
    }

    s+= "******\n"+
    "Series:\n"+
    "******\n";

    for (int i = 0; i < this.series.length; i++) {
      if (this.series[i] != null) s+= this.series[i].toString();
    }

    return s;

	}
}

/*


C) Implementa la clase ElPuigMedia:  (5 puntos)
La classe ElPuigMedia representa un servicio de préstamo de productos digitales, en nuestro caso Videojuegos y Series.

Los atributos de esta clase corresponden a dos arrays, uno de Series y otro de Videojuegos, de 10 posiciones cada uno. Además tendrá un atributo que indicará el tiempo máximo de los préstamos en días, por defecto 10 días.

Los constructores que se implementarán serán:
Un constructor por defecto. (Sin parámetros)
Un constructor con los dos arrays uno de Series y otro de videojuegos. El resto los valores por defecto.
Un constructor con todos los atributos.

Los métodos que se implementara serán:
Métodos get de todos los atributos.
Métodos set de todos los atributos.
añadirVideojuego(Videojuego: vj): Añade un Videojuego a la colección de Videojuegos.
añadirSerie(Serie: s): Añade una Serie a la colección de Series.
entregarSerie(int: i): Entrega la Serie en la posición i del array.
devolverSerie(int: i): Devuelve la Serie en la posición i del array.
entregarVideojuego(int: i): Entrega el Videojuego en la posición i del array.
devolverVideojuego(int: i): Devuelve el Videojuego en la posición i del array.
totalSeriesEntregadas(): Devuelve el total de Series entregadas.
totalVideojuegosEntregados(): Devuelve el total de Videojuegos entregados.
getVideoJuegoConMasHoras(): Devuelve el Videojuego con más horas estimadas.
getSerieConMasTemporadas(): Devuelve la Serie que tiene más temporadas.
Sobrescribe el método toString con el siguiente formato:

--------------------------- EL PUIG MEDIA ---------------------------
***********************
Tiempo máximo préstamos: <VALOR_ATRIBUTO_DIAS> días
***********************
********
Catálogo:
********
***********
Videojuegos:
***********
Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Compañía:
<VALOR_ATRIBUTO_COMPAÑIA>
Horas estimadas:
<VALOR_ATRIBUTO_HORAS_ESTIMADAS>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)

******
Series:
******
Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Temporadas:
<VALOR_ATRIBUTO_TEMPORADAS>
Creador:
<VALOR_ATRIBUTO_CREADOR>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)


* Llama a los métodos toString para imprimir el catálogo.


*/
